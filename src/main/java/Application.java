public class Application {

    public static void main(String[] args) {
        Client client = new Client("Martin", "Kelig", 47, 123);
        BankAccount bankAccount = new BankAccount(1000, 456, client);

        System.out.println(bankAccount.getSolde());
        bankAccount.deposer(100);
        System.out.println(bankAccount.getSolde());
        bankAccount.retirer(10000);
        System.out.println(bankAccount.getSolde());

    }
}
