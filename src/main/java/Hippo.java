public class Hippo extends Animal {

    private int power;

    public Hippo(int weight, int height, int name, int power) {
        super(weight, height, name);
        this.power = power;
    }

    public Hippo swim() {
        // fais perdre du poids à l'hippopotame
        return null;
    }

    public Hippo eat() {
        // fais prendre du poids
        return null;
    }


    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
